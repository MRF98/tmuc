﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder
{
    public class Pizza
    {
        public string Crust { get; set; }
        public string Toppings { get; set; }
        public string Addons { get; set; }
        public string Flavor { get; set; }


        public Pizza(string _crust, string _toppings, string _addons,string _flavor)
        {
            this.Crust = _crust;
            this.Toppings = _toppings;
            this.Addons = _addons;
            this.Flavor = _flavor;
        }

        public Pizza()
        {

        }
    }

    public class PizzaBuilder
    {
        private Pizza pizza;
        public Pizza SetCrust(string crust)
        {
            pizza.Crust = crust;
            return pizza;
        }
        public Pizza SetToppings(string toppings)
        {
            pizza.Toppings = toppings;
            return pizza;
        }
        public Pizza SetAddons(string addons)
        {
            pizza.Addons = addons;
            return pizza;
        }
        public Pizza SetFlavor(string flavor)
        {
            pizza.Flavor = flavor;
            return pizza;
        }

        public void GetPizza()
        {
            Console.WriteLine("Pizza details are : Crust = " + pizza.Crust + ", Toppings = " + pizza.Toppings + ", Addons = " + pizza.Addons+", Flavor = "+pizza.Flavor);
        }

        public PizzaBuilder()
        {
            pizza = new Pizza();
        }
    }

    class Program
    {
        public static void Main()
        {
            Pizza pz1 = new Pizza("Stuffed Crust", "Pepporini", "Garlic Bread", "Chilli Chicken");
            PizzaBuilder pz2 = new PizzaBuilder();
            pz2.SetCrust("Pan Pizza");
            pz2.SetToppings("Cheese");
            pz2.SetAddons("Garlic Bread");
            pz2.SetFlavor("Chilli Chicken");
            pz2.GetPizza();
            Console.ReadKey();
        }
    }
}