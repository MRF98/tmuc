﻿using System;

namespace Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            PlainPizza ppObj = new PlainPizza();
            string pp = ppObj.MakePizza();
            Console.WriteLine(pp);

            PizzaDecorator chickenPizzaDecorator = new chickenPizzaDecorator(ppObj);
            string chickenPizza =  chickenPizzaDecorator.MakePizza();
            Console.WriteLine("\n'" + chickenPizza);

            VegetablePizzaDecorator vegPizzaDecorator = new VegetablePizzaDecorator(ppObj);
            string vegPizza = vegPizzaDecorator.MakePizza();
            Console.WriteLine("\n'" + vegPizza);

            PepperonipizzaDecorator pepperoniPizzaDecorator = new PepperonipizzaDecorator(ppObj);
            string peppizza = pepperoniPizzaDecorator.MakePizza();
            Console.WriteLine("\n'" + peppizza);
        }
        public interface Pizza
        {
            string MakePizza();
        }
        public class PlainPizza : Pizza
        {
            public string MakePizza()
            {
                return "Plain Pizza";
            }
        }
        public abstract class PizzaDecorator : Pizza
        {
            protected Pizza pizza;
            public PizzaDecorator(Pizza pizza)
            {
                this.pizza = pizza;
            }
            public virtual string MakePizza()
            {
                return pizza.MakePizza();
            }
        }
        public class chickenPizzaDecorator : PizzaDecorator
        {
            public chickenPizzaDecorator(Pizza pizza) : base(pizza)
            {
            }
            public override string MakePizza()
            {
                return pizza.MakePizza() + AddChicken() + AddExtraSauce();  
            }
            private string AddChicken()
            {
                return ", Chicken added ";
            }
            private string AddExtraSauce()
            {
                return ", Extra Sauce added";
            }

        }
        public class VegetablePizzaDecorator : PizzaDecorator
        {
            public VegetablePizzaDecorator(Pizza pizza) : base(pizza)
            { 
            }
            public override string MakePizza()
            {
                return pizza.MakePizza() + AddVegetables() + AddExtraCheese();
            }
            private string AddVegetables()
            {
                return ", vegetables added";
            }
            private string AddExtraCheese()
            {
                return ", Extra cheese added";
            }
        }
        public class PepperonipizzaDecorator : PizzaDecorator
        {
            public PepperonipizzaDecorator(Pizza pizza) : base (pizza)
            {
            }
            public override string MakePizza()
            {
                return pizza.MakePizza() + AddPepperoni();

            }
            private string AddPepperoni()
            {
                return ", Pepperoni added";
            }
        }
       
    }
}
